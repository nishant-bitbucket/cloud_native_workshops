Objective:

To complete objectives for this case, you'll need to complete case-1 and case-2 scenarios from the workshop.

Architecture: https://lucid.app/lucidchart/028ffdf6-1088-4ee9-9371-0297260f31b4/edit?invitationId=inv_3e64cba2-8c73-4044-9713-7810c24c886e

1. Designing network for the application requirement

Thought process:
- Prepare terraform to create a VPC as per above network design diagram
- Make sure NACLs across subnets are restricted and allow only specific ports as per application requirement (understand the stateless behavior of NACL)
- Make sure you understand difference between DMZ, app and db subnets ( Why do we have these three layers and how are they related to public and private subnets?) 
- What is an air gapped network and why do we need it ?


2. Define security Groups 

Thought process:
- What port is required to be whitelisted in application security group? What is whitelisting and blacklisting?


3. Create EC2 instance in DMZ subnet using terraform and deploy the application on it (deploy using linux service on port 8000 and deploy as docker on port 9000)

Thought process:
- In this exercise Database needs to be running on same ec2 instances,w e'll separate them in advance labs

4. Deploy a new version using CD methodology (need not be 100% automated but basically there has to be a build and release job) atleast that can be triggered to do build or deployments
Thought process:
- Deploy new versions
- Check accessibility of application on public IP of Ec2 instance
- Make a host entry in /etc/hosts in your local machine to access it with a domain (read about how you can make this entry and how DNS works)
- Make a route 53 entry ( A record for the Public IP of the instance)

5. Prepare notes and diagrams around how this system works in CI/CD fashion

6. Understand the difference between protocols like SSH,DNS,HTTP 
Thought process:
- How do these protocols are different compared using ports? Can they run on custom ports that we define? (Time to quickly read Julia Evans WizardZines)
- Whats the difference between devops users and application users 