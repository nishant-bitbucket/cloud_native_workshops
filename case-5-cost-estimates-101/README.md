Prerequisite:
Complete the following scenarios
case-1
case-2
case-3
case-4

Objective:
Learning cost estimates to become a better architect. 

1. Use AWS cost calculator to estimate the cost of running the system in case-3

Thought process:
- Read about cost impacts when it comes to running same services in different regions in AWS
- What are data costs? Does data transfer has an associated costs in AWS ? How does it impact design decisions ?

2. Prepare cost estimates for running all the systems in case-4

Thought process:
- Think carefully around preparing it in excel sheets
- Make information easy to understand and explain with a PPT 
- Learn information packaging and impact of communication on client engagements