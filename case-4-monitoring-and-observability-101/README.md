Prerequisite:
Complete the following scenarios
case-1
case-2
case-3

Objective:


Architecture: https://lucid.app/lucidchart/5620b5e8-278e-4fe3-b043-4c9f89da96b3/edit?invitationId=inv_2f2b8d82-8360-424f-9b99-7d8c457bd7b4

1. Understand the evolution of the architecture

Thought process:
- What changes have been introduced in the architecture compared to case-2?
- What is the reason of this new change (a business requirement?)
- Why only specific platform is introduced? Can we question this change with a better platform?
- Prepare notes around why do we need monitoring and observability in the system ? Can this be achieved only using AWS ecosystem of services (focus on goals and objective?

2. Centralized metrics: What are the type of metrics we can send from application to NewRelic ?

Thought process:
- Learn New relic connectors for metrics and sen the metric from your application/infrastructure?
- Can we sen some custom metrics to NewRelic?
- Can we also monitor our local jenkins/bamboo using NewRelic?


3. Centralized logs: What are the type of logs we can send from application to NewRelic ?

Thought process:
- Learn what are the important logs from host OS or docker container we are running?
- Ship them to new Relic

4. Alerting: Generate sample alerts based on some conditions 

Thought process:
- Integrate Newrelic, Pagerduty and Slack ( Always pay attention to small steps that you can perform to test these integrations)
- What are the networking requirements for these integrations?
- What are the application authentication procedure used for these integrations?
- Based on certain threashold on logs or metrics can we generate an event/alert that triggers pagerduty alert or slack message in a channel ? WHy its useful for the SRE/DevOps teams 

5. Read and recite below content multiple times
- https://github.com/shibumi/SRE-cheat-sheet
- https://github.com/anshudutta/sre-cheat-sheet

Thought process:
- Prepare your own notes around SRE as you are starting to understand it
- How can you monitor a system to reach relibility goals ? How is it related in our case with tools like NewRelic, PagerDuty and Slack ?
- Does SLA of any system impact design of it ? How does it become a tradeoff between cost of building a system?
- 


